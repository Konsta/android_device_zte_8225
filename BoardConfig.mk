USE_CAMERA_STUB := true

# inherit from the proprietary version
-include vendor/zte/8225/BoardConfigVendor.mk

TARGET_NO_BOOTLOADER := true

TARGET_BOARD_PLATFORM := msm7x27a
TARGET_BOARD_PLATFORM_GPU := qcom-adreno203

TARGET_ARCH := arm
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
ARCH_ARM_HAVE_TLS_REGISTER := true

TARGET_PREBUILT_KERNEL := device/zte/8225/recovery/kernel

TARGET_BOOTLOADER_BOARD_NAME := 8225
BOARD_KERNEL_CMDLINE := console=ttyHSL0,115200,n8 androidboot.hardware=qcom vmalloc=200M
BOARD_KERNEL_BASE := 0x00200000
BOARD_KERNEL_PAGESIZE := 4096

# fix this up by examining /proc/mtd on a running device
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 0x105c0000
BOARD_FLASH_BLOCK_SIZE := 131072

# Recovery
BOARD_CUSTOM_GRAPHICS := ../../../device/zte/8225/recovery/graphics.c
TARGET_RECOVERY_INITRC := device/zte/8225/recovery/recovery.rc
TARGET_RECOVERY_FSTAB := device/zte/8225/recovery/recovery.fstab
TARGET_PREBUILT_RECOVERY_KERNEL := device/zte/8225/recovery/kernel
BOARD_HAS_NO_SELECT_BUTTON := true
BOARD_UMS_LUNFILE := "/sys/class/android_usb/f_mass_storage/lun1/file"

